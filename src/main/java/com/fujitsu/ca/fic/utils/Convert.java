package com.fujitsu.ca.fic.utils;

import java.util.List;

import org.apache.mahout.math.Matrix;
import org.apache.mahout.math.NamedVector;
import org.apache.mahout.math.SparseMatrix;
import org.apache.mahout.math.Vector;

public class Convert {
    public static Matrix fromNamedVectorsToMatrix(List<NamedVector> listOfNamedVectors, int vectorSize) {
        Matrix examplesMatrix = new SparseMatrix(listOfNamedVectors.size(), vectorSize);

        int row = 0;
        for (NamedVector vector : listOfNamedVectors) {
            examplesMatrix.assignRow(row++, vector);
        }
        return examplesMatrix;
    }

    public static Matrix fromVectorsToMatrix(List<Vector> listOfVectors, int vectorSize) {
        Matrix examplesMatrix = new SparseMatrix(listOfVectors.size(), vectorSize);

        int row = 0;
        for (Vector vector : listOfVectors) {
            examplesMatrix.assignRow(row++, vector);
        }
        return examplesMatrix;
    }
}
