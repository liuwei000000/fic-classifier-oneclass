package com.fujitsu.ca.fic.dataloaders.kdd1999;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.PathFilter;
import org.apache.mahout.math.NamedVector;
import org.apache.mahout.math.SequentialAccessSparseVector;
import org.apache.mahout.math.Vector;

import com.fujitsu.ca.fic.dataloaders.DynamicDatasetLoader;
import com.fujitsu.ca.fic.exceptions.IncorrectLineFormatException;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

public class DynamicKddLoader implements DynamicDatasetLoader {
    protected BiMap<Integer, String> categories;

    private final static int FEATURES = 41;
    private final static int CATEGORIES = 2;
    private final static int LABEL_INDEX = 0;
    private final static String POSITIVE_LABEL = "1";

    private FileSystem fs;
    private FileStatus[] fileStatus;
    private int currentFileStatusIndex = 0;
    private BufferedReader reader = null;
    private String nextLine = null;

    public DynamicKddLoader(Configuration conf, String pathName) throws IOException {
        categories = HashBiMap.create(CATEGORIES);
        categories.put(0, "attack");
        categories.put(1, "normal");

        fs = FileSystem.get(conf);
        fileStatus = fs.listStatus(new Path(pathName), new PathFilter() {
            @Override
            public boolean accept(Path path) {
                return path.getName().matches("part(.*)");
            }
        });

        if (fileStatus.length > 0) {
            FileStatus file = fileStatus[currentFileStatusIndex];
            reader = new BufferedReader(new InputStreamReader(fs.open(file.getPath())));
        }
    }

    @Override
    public int getFeaturesCount() {
        return FEATURES;
    }

    @Override
    public int getCategoriesCount() {
        return CATEGORIES;
    }

    @Override
    public String getPositiveLabel() {
        return POSITIVE_LABEL;
    }

    @Override
    public boolean hasNext() {
        if (reader == null)
            return false;
        try {
            nextLine = reader.readLine();
            if (!currentFileHasNextLine(nextLine) && directoryHasMoreFiles()) {
                FileStatus file = fileStatus[++currentFileStatusIndex];
                reader = new BufferedReader(new InputStreamReader(fs.open(file.getPath())));
                hasNext();
            } else if (currentFileHasNextLine(nextLine)) {
                return true;
            }
            reader.close();

        } catch (IOException e) {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return false;
    }

    private boolean directoryHasMoreFiles() {
        return currentFileStatusIndex + 1 < fileStatus.length;
    }

    private boolean currentFileHasNextLine(String nextLine2) {
        return (nextLine != null && nextLine.length() > 0);
    }

    @Override
    public NamedVector getNext() throws IncorrectLineFormatException {
        return parseFields(nextLine);
    }

    public NamedVector parseFields(String line) throws IncorrectLineFormatException {
        final int FIRST_FEATURE_INDEX = 1;
        double[] featuresDouble = new double[FEATURES];
        String[] features;

        try {
            features = line.split(",");

            for (int i = 0; i < FEATURES; i++) {
                featuresDouble[i] = Double.parseDouble(features[i + FIRST_FEATURE_INDEX]);
            }
        } catch (Exception e) {
            throw new IncorrectLineFormatException("Could not parse line: \n" + line);
        }

        Vector featureVector = new SequentialAccessSparseVector(FEATURES);
        featureVector.assign(featuresDouble);
        return new NamedVector(featureVector, features[LABEL_INDEX]);
    }
}
