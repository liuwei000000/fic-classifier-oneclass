package com.fujitsu.ca.fic.dataloaders;

import java.io.IOException;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.mahout.math.NamedVector;

public interface DatasetFileLoader {
    List<NamedVector> load(Configuration conf, String filePath) throws IOException;

    List<NamedVector> load(Configuration conf, String trainPositivesPath, int maxResults) throws IOException;

    String getPositiveLabel();

    int getFeaturesCount();

    int getCategoriesCount();

    String getLabelName(int category);

    int getLabelValue(String name);

    // TODO MATHIEU need to add a way to get the symbols i.e. getSymbols();
}
