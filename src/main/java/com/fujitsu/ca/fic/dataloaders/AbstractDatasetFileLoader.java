package com.fujitsu.ca.fic.dataloaders;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.PathFilter;
import org.apache.mahout.math.NamedVector;

import com.fujitsu.ca.fic.exceptions.IncorrectLineFormatException;
import com.google.common.collect.BiMap;
import com.google.common.collect.Lists;

public abstract class AbstractDatasetFileLoader implements DatasetFileLoader {
    protected BiMap<Integer, String> categories;

    @Override
    public List<NamedVector> load(Configuration conf, String filePath) throws IOException {
        return load(conf, filePath, Integer.MAX_VALUE);
    }

    @Override
    public List<NamedVector> load(Configuration conf, String filePath, int maxResults) throws IOException {
        FileSystem fs = FileSystem.get(conf);
        FileStatus[] fileStatus = fs.listStatus(new Path(filePath), new PathFilter() {
            @Override
            public boolean accept(Path path) {
                return path.getName().matches("part(.*)");
            }
        });

        BufferedReader reader = null;
        List<NamedVector> vectorList = Lists.newArrayList();

        int linesReadCounter = 0;
        try {
            for (FileStatus file : fileStatus) {
                reader = new BufferedReader(new InputStreamReader(fs.open(file.getPath())));
                String line = reader.readLine();
                while (line != null && line.length() > 0 && linesReadCounter < maxResults) {
                    NamedVector featureVector;
                    featureVector = parseFields(line);
                    vectorList.add(featureVector);
                    line = reader.readLine();
                }
            }
            ++linesReadCounter;
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
        return vectorList;
    }

    @Deprecated
    public List<NamedVector> loadPositives(Configuration conf, String filePath) throws IOException {
        BufferedReader reader = null;
        FSDataInputStream fsDataInputStream = null;
        List<NamedVector> vectorList = Lists.newArrayList();

        try {
            FileSystem fs = FileSystem.get(conf);
            fsDataInputStream = fs.open(new Path(filePath));
            reader = new BufferedReader(new InputStreamReader(fsDataInputStream));

            String line = reader.readLine();
            while (line != null && line.length() > 0) {
                NamedVector featureVector;
                featureVector = parseFields(line);

                if (isPositive(featureVector)) {
                    vectorList.add(featureVector);
                }
                line = reader.readLine();
            }
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
        return vectorList;
    }

    public boolean isPositive(NamedVector example) {
        return example.getName().equals(getPositiveLabel());
    }

    public abstract NamedVector parseFields(String line) throws IncorrectLineFormatException;

    @Override
    public String getLabelName(int category) {
        return categories.get(category);
    }

    @Override
    public int getLabelValue(String name) {
        return categories.inverse().get(name);
    }
}
