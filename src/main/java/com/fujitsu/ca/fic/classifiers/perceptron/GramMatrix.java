package com.fujitsu.ca.fic.classifiers.perceptron;

import org.apache.mahout.math.DenseMatrix;
import org.apache.mahout.math.Matrix;

public class GramMatrix {
    private final Matrix gram;
    private final int size;

    public GramMatrix(Matrix positiveDataset) {
        size = positiveDataset.numRows();
        gram = new DenseMatrix(size, size);

        for (int row1 = 0; row1 < size; row1++) {
            for (int row2 = 0; row2 < size; row2++) {
                gram.set(row1, row2, dotProduct(positiveDataset, row1, row2));
            }
        }
    }

    public double at(int row, int column) {
        return gram.get(row, column);
    }

    private double dotProduct(Matrix dataset, int row1, int row2) {
        return dataset.viewRow(row1).dot(dataset.viewRow(row2));
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < gram.rowSize(); i++) {
            for (int j = 0; j < gram.columnSize(); j++) {
                sb.append(gram.get(i, j) + " ");
            }
            sb.append("\n");
        }
        sb.append("\n");
        return sb.toString();
    }

}
