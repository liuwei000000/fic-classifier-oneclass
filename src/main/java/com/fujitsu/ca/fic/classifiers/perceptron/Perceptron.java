package com.fujitsu.ca.fic.classifiers.perceptron;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.mahout.classifier.AbstractVectorClassifier;
import org.apache.mahout.common.HadoopUtil;
import org.apache.mahout.math.DenseVector;
import org.apache.mahout.math.Matrix;
import org.apache.mahout.math.NamedVector;
import org.apache.mahout.math.SparseRowMatrix;
import org.apache.mahout.math.Vector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fujitsu.ca.fic.utils.Convert;

public class Perceptron extends AbstractVectorClassifier {
    private static final float GAMMA_DEFAULT_VALUE = 0.00001f;
    @SuppressWarnings("unused")
    private static final float C_DEFAULT_VALUE = 1.0f;

    private static Logger LOG = LoggerFactory.getLogger(Perceptron.class);

    // private GramMatrix gram;
    private RBFKernel kernel;

    private Matrix positiveDataset;
    private Vector alpha;
    private double gamma;
    private GramMatrix gram;

    public static Perceptron buildTrainedPerceptron(Configuration conf, List<NamedVector> positiveExamples) {
        if (positiveExamples.isEmpty()) {
            String message = "The perceptron cannot be trained with empty positivesExamples list!";
            LOG.error(message);
            throw new RuntimeException(message);
        }
        double gamma = conf.getFloat("perceptron.gamma", GAMMA_DEFAULT_VALUE);
        Vector alpha = new DenseVector(positiveExamples.size());
        alpha.assign(1);
        Perceptron p = new Perceptron(positiveExamples, gamma, alpha);
        return p;
    }

    public Perceptron(List<NamedVector> positiveExamples, double gamma, Vector alpha) {
        int featureCount = positiveExamples.get(0).size();
        this.kernel = new RBFKernel(gamma);
        this.gamma = gamma;
        this.alpha = alpha;
        this.positiveDataset = Convert.fromNamedVectorsToMatrix(positiveExamples, featureCount);
    }

    @Deprecated
    private Perceptron(List<NamedVector> positiveExamples, double gamma) {
        Collections.shuffle(positiveExamples);

        int featureCount = positiveExamples.get(0).size();
        this.kernel = new RBFKernel(gamma);
        this.gamma = gamma;

        this.positiveDataset = Convert.fromNamedVectorsToMatrix(positiveExamples, featureCount);
        this.gram = new GramMatrix(positiveDataset);
    }

    /**
     * Deprecated because the perceptron was found to have an alpha vector always equal to: 1,0,0,... There is no point to train it. Use
     * buildTrainedPerceptron(Configuration conf, List<NamedVector> positiveExamples) instead.
     * 
     * @param positiveExamples
     * @param C
     * @param gamma
     * @return
     */
    @Deprecated
    public static Perceptron buildTrainedPerceptron(List<NamedVector> positiveExamples, double C, double gamma) {
        if (positiveExamples.isEmpty()) {
            String message = "The perceptron cannot be trained with empty positivesExamples list!";
            LOG.error(message);
            throw new RuntimeException(message);
        }
        Perceptron p = new Perceptron(positiveExamples, gamma);
        p.train(C);
        return p;
    }

    /**
     * Does not really train anything, given only positive examples, the first element of alpha will be 1, then nothing else will be
     * updated. Recommended way to create Perceptron is by using buildTrainedPerceptron(Configuration conf, List<NamedVector>
     * positiveExamples)
     * 
     * @param C
     */
    @Deprecated
    public void train(double C) {
        int numRowsInPositiveDataset = positiveDataset.numRows();
        Vector K = new DenseVector(numRowsInPositiveDataset);
        K.assign(0.0);
        alpha = new DenseVector(numRowsInPositiveDataset);
        alpha.assign(1);

        boolean wasUpdated;
        do {
            wasUpdated = false;
            for (int i = 0; i < numRowsInPositiveDataset; i++) {
                if (K.get(i) <= 0 && alpha.get(i) < C) {

                    alpha.set(i, alpha.get(i) + 1);
                    wasUpdated = true;
                    for (int j = 0; j < numRowsInPositiveDataset; j++) {
                        K.set(j, K.get(j) + gram.at(i, j));
                    }
                }
            }

        } while (wasUpdated);
    }

    @Override
    public double classifyScalar(Vector example) {
        double sums = 0.0;

        for (int j = 0; j < positiveDataset.numRows(); j++) {
            sums += alpha.get(j) * kernel.calculateScalarProduct(positiveDataset.viewRow(j), example);
        }
        return sums;
    }

    @Override
    public int numCategories() {
        return 2;
    }

    @Override
    public Vector classify(Vector example) {
        // Will never happen, since a perceptron is a binary classificator.
        // Nevertheless, implements classifyScalar with a Vector as the return value.
        Vector classification = new DenseVector(1);

        classification.set(0, classifyScalar(example));
        return classification;
    }

    @Deprecated
    public Perceptron(Configuration conf, Path path) throws IOException {
        FSDataInputStream input = null;

        try {
            FileSystem fs = FileSystem.get(conf);
            input = fs.open(path);

            initializeWithBinary(input);
        } finally {
            if (input != null) {
                input.close();
            }
        }

        kernel = new RBFKernel(gamma);
    }

    public static Perceptron loadFromFile(Configuration conf) throws IOException {
        return new Perceptron(conf);
    }

    private Perceptron(Configuration conf) throws IOException {
        FSDataInputStream input = null;

        try {
            String perceptronModelPath = conf.get("perceptron.model.path");
            if (perceptronModelPath == null) {
                String message = "the perceptron model path could not be read from configuration!";
                LOG.error(message);
                throw new RuntimeException(message);
            }

            FileSystem fs = FileSystem.get(conf);
            input = fs.open(new Path(perceptronModelPath));

            initializeWithBinary(input);
        } finally {
            if (input != null) {
                input.close();
            }
        }

        kernel = new RBFKernel(gamma);
    }

    private void initializeWithBinary(FSDataInputStream input) throws IOException {
        gamma = input.readDouble();

        int alphaSize = input.readInt();
        alpha = new DenseVector(alphaSize);
        for (int row = 0; row < alpha.size(); row++) {
            alpha.set(row, input.readDouble());
        }

        int rowSize = input.readInt();
        int colSize = input.readInt();
        positiveDataset = new SparseRowMatrix(rowSize, colSize);
        for (int row = 0; row < positiveDataset.numRows(); row++) {
            for (int col = 0; col < positiveDataset.numCols(); col++) {
                positiveDataset.set(row, col, input.readDouble());
            }
        }
    }

    public void saveToFile(Configuration conf) throws IOException {
        String modelPathString = conf.get("perceptron.model.path");
        if (modelPathString == null) {
            String message = "the perceptron model path could not be read from configuration!";
            LOG.error(message);
            throw new RuntimeException(message);
        }

        FSDataOutputStream output = null;
        Path modelPath = new Path(modelPathString);

        try {
            HadoopUtil.delete(conf, modelPath);
            FileSystem fs = FileSystem.get(conf);
            output = fs.create(modelPath);

            output.writeDouble(gamma);
            output.writeInt(alpha.size());
            for (int row = 0; row < alpha.size(); row++) {
                output.writeDouble(alpha.get(row));
            }
            output.writeInt(positiveDataset.numRows());
            output.writeInt(positiveDataset.numCols());
            for (int row = 0; row < positiveDataset.numRows(); row++) {
                for (int col = 0; col < positiveDataset.numCols(); col++) {
                    output.writeDouble(positiveDataset.get(row, col));
                }
            }
        } finally {
            if (output != null) {
                output.close();
            }
        }
    }

    public void setGamma(double gamma) {
        this.kernel = new RBFKernel(gamma);
    }
}
