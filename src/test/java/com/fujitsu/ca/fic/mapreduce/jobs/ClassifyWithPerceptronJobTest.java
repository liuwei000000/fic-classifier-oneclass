package com.fujitsu.ca.fic.mapreduce.jobs;

import java.io.IOException;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.mahout.common.HadoopUtil;
import org.apache.mahout.math.NamedVector;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fujitsu.ca.fic.classifiers.perceptron.Perceptron;
import com.fujitsu.ca.fic.dataloaders.AbstractDatasetFileLoader;
import com.fujitsu.ca.fic.dataloaders.kdd1999.KddLoader;

import static org.hamcrest.CoreMatchers.is;

import static org.hamcrest.MatcherAssert.assertThat;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class ClassifyWithPerceptronJobTest {
    private static final double GAMMA = 1;
    private static FileSystem fs;
    static Configuration conf = new Configuration();
    Perceptron perceptron;
    AbstractDatasetFileLoader dataLoader = new KddLoader();

    @BeforeClass
    public static void setUp() throws Exception {
        fs = FileSystem.get(conf);
        conf.addResource(fs.open(new Path("conf/local-test-conf-1.xml")));
    }

    @Test
    public void configurationFileReadCorrectly() throws IOException {
        assertThat(conf.get("data.train-positives.path"), is("data/test/kdd1999/train-positives-small"));
        assertThat(conf.get("perceptron.model.path"), is("model/test/perceptron.model"));
        assertThat(conf.get("data.perceptron.output.path"), is("data/test/perceptron/output"));
    }

    @Test
    public void buildingTrainedPerceptronShouldModelFileToLocation() throws Exception {
        conf.set("perceptron.gamma", Double.toString(GAMMA));
        String trainPath = conf.get("data.train-positives.path");
        List<NamedVector> trainingExamples = dataLoader.load(conf, trainPath);

        String perceptronModelPath = conf.get("perceptron.model.path");
        HadoopUtil.delete(conf, new Path(perceptronModelPath));
        perceptron = Perceptron.buildTrainedPerceptron(conf, trainingExamples);
        perceptron.saveToFile(conf);

        assertTrue(fs.exists(new Path(perceptronModelPath)));
    }

    @Test
    public void jobShouldNotThrowException() throws Exception {
        conf.set("perceptron.gamma", Double.toString(GAMMA));
        try {
            String trainPath = conf.get("data.train-positives.path");
            List<NamedVector> trainingExamples = dataLoader.load(conf, trainPath);

            String perceptronModelPath = conf.get("perceptron.model.path");
            HadoopUtil.delete(conf, new Path(perceptronModelPath));
            perceptron = Perceptron.buildTrainedPerceptron(conf, trainingExamples);
            perceptron.saveToFile(conf);

            String jobOutputPath = conf.get("data.perceptron.output.path");
            HadoopUtil.delete(conf, new Path(jobOutputPath));

            ClassifyWithPerceptronJob classifyWithPerceptronJob = new ClassifyWithPerceptronJob(conf);
            classifyWithPerceptronJob.scoreUnlabeledWithPerceptron();

        } catch (Exception e) {
            fail("Threw exception: " + e.getMessage());
        }
    }

}
