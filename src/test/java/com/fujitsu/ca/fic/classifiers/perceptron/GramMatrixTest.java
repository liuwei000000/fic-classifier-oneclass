package com.fujitsu.ca.fic.classifiers.perceptron;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.apache.mahout.math.Matrix;
import org.apache.mahout.math.Vector;
import org.junit.Before;
import org.junit.Test;

import com.fujitsu.ca.fic.classifiers.perceptron.GramMatrix;

public class GramMatrixTest {
    private final int DATASET_SIZE = 2;
    private final int ROW1 = 0;
    private final int ROW2 = 1;
    private final double DOT_PRODUCT1 = 123.321;
    private final double DOT_PRODUCT23 = 321.123;
    private final double DOT_PRODUCT4 = 42.31416;
    private final double EPSILON = 0.0000001;

    private GramMatrix gramMatrix;

    private Matrix aPositiveDataset;
    private Vector row1Vector;
    private Vector row2Vector;

    @Before
    public void setup() {
        row1Vector = mock(Vector.class);
        row2Vector = mock(Vector.class);
        aPositiveDataset = mock(Matrix.class);

        setupPositiveDataset();
        setupDotProducts();

        gramMatrix = new GramMatrix(aPositiveDataset);
    }

    @Test
    public void shouldBuildADotProductsMatrixWhenUsingAPositiveDataset() {
        assertEquals(DOT_PRODUCT1, gramMatrix.at(ROW1, ROW1), EPSILON);
        assertEquals(DOT_PRODUCT23, gramMatrix.at(ROW2, ROW1), EPSILON);
        assertEquals(DOT_PRODUCT23, gramMatrix.at(ROW1, ROW2), EPSILON);
        assertEquals(DOT_PRODUCT4, gramMatrix.at(ROW2, ROW2), EPSILON);
    }

    private void setupPositiveDataset() {
        when(aPositiveDataset.numRows()).thenReturn(DATASET_SIZE);
        when(aPositiveDataset.viewRow(ROW1)).thenReturn(row1Vector);
        when(aPositiveDataset.viewRow(ROW2)).thenReturn(row2Vector);
    }

    private void setupDotProducts() {
        when(row1Vector.dot(row1Vector)).thenReturn(DOT_PRODUCT1);
        when(row1Vector.dot(row2Vector)).thenReturn(DOT_PRODUCT23);
        when(row2Vector.dot(row1Vector)).thenReturn(DOT_PRODUCT23);
        when(row2Vector.dot(row2Vector)).thenReturn(DOT_PRODUCT4);
    }

}
