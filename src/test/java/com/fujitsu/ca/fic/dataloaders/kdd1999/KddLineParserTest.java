package com.fujitsu.ca.fic.dataloaders.kdd1999;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.fujitsu.ca.fic.dataloaders.LineParser;
import com.fujitsu.ca.fic.exceptions.IncorrectLineFormatException;

public class KddLineParserTest {
    String line1 = "0,0,1,53,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,146,20,0.0,0.0,1.0,1.0,0.14,0.06,0.0,255.0,20.0,0.08,0.06,0.0,0.0,0.0,0.0,1.0,1.0";
    String line2 = "1,0,1,12,1,285,1184,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,24,34,0.0,0.0,0.0,0.0,1.0,0.0,0.09,225.0,255.0,1.0,0.0,0.0,0.02,0.0,0.0,0.0,0.0";
    String badFormatLine = "1,0,1,12,1,285,1184,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,24,34,0.0,0.0,0.0,1.0,0.0,0.09,225.0,255.0,1.0,0.0,0.0,0.02,0.0,0.0,0.0,0.0";

    LineParser lineParser = new KddLineParser();

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void givenKddExampleNoExceptionThrown() throws IncorrectLineFormatException {
        lineParser.parseFields(line1);
    }

    @Test
    public void givenBadFormatExceptionThrown() {
        try {
            lineParser.parseFields(badFormatLine);
            fail("No exception was thrown!");
        } catch (IncorrectLineFormatException e) {
            assertTrue(true);
        }
    }

    // TODO more tests?
}
