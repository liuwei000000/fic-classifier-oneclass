package com.fujitsu.ca.fic.dataloaders.kdd1999;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.mahout.math.NamedVector;
import org.junit.Test;

import com.fujitsu.ca.fic.dataloaders.DynamicDatasetLoader;
import com.fujitsu.ca.fic.exceptions.IncorrectLineFormatException;

import static org.hamcrest.MatcherAssert.assertThat;

import static org.hamcrest.Matchers.isOneOf;

public class DynamicKddLoaderTest {
    private final static String ONE_FILE_KDD = "data/test/kdd1999/kddtiny";
    private final static String TWO_FILES_KDD = "data/test/kdd1999/2files";
    private final static String THREE_FILE_KDD = "data/test/kdd1999/3files";
    Configuration conf = new Configuration();

    @Test
    public void checkTrain3PartsOK() throws IOException {
        DynamicDatasetLoader loader = new DynamicKddLoader(conf, THREE_FILE_KDD);
        checkDatasetWithLoader(loader);
    }

    @Test
    public void checkTest2PartsOK() throws IOException {
        DynamicDatasetLoader loader = new DynamicKddLoader(conf, TWO_FILES_KDD);
        checkDatasetWithLoader(loader);
    }

    @Test
    public void checkTest1PartOK() throws IOException {
        DynamicDatasetLoader loader = new DynamicKddLoader(conf, ONE_FILE_KDD);
        checkDatasetWithLoader(loader);
    }

    private void checkDatasetWithLoader(DynamicDatasetLoader loader) throws IncorrectLineFormatException {
        while (loader.hasNext()) {
            NamedVector nextExample;
            nextExample = loader.getNext();
            String labelName = nextExample.getName();
            Integer actual = Integer.parseInt(labelName);
            assertThat(actual, isOneOf(0, 1));
        }
    }
}
